<?php

namespace Drupal\search_api_grouping\Plugin\search_api\processor;

use Drupal\Core\Form\FormStateInterface;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\search_api\Item\Item;
use Drupal\search_api\Processor\FieldsProcessorPluginBase;

/**
 * This processor allows you to configure which multivalue fields are used for denormalization.
 *
 * (see https://issues.apache.org/jira/browse/SOLR-10894 and
 * https://mail-archives.apache.org/mod_mbox/lucene-solr-user/201805.mbox/%3cCAE4tqLPXMDA8y3hzXXkJUtTm6jvUX8XZ0H6P5itcFPgmr1bQZA@mail.gmail.com%3e)
 *
 * @SearchApiProcessor(
 *   id = "denormalization",
 *   label = @Translation("Denormalization"),
 *   description = @Translation("This processor allows you to configure which multivalue fields are used for denormalization."),
 *   stages = {
 *     "add_properties" = 0,
 *     "alter_items" = -10,
 *   },
 * )
 */
class Denormalize extends FieldsProcessorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $configuration = parent::defaultConfiguration();

    $configuration += [
      'permutation_limit' => NULL,
      'denormalization_field' => ''
    ];

    return $configuration;
  }

  /**
   * Return the settings form for this processor.
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['permutation_limit'] = array(
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#title' => t('Permutation limits'),
      '#description' => t('Defines how man permutations should be processed. Can become handy if you just want to have one item of a muti-value field. Leave empty for no limit.'),
      '#weight' => 1,
    );

    $options = array();
    foreach ($form['fields']['#options'] as $field_name => $label) {
      if (stristr($field_name, ':')) {
        list($field_name, $property) = explode(':', $field_name, 2);
      }
      if(stristr($label, "»")){
        list($type, $label) = explode('»', $label, 2);
      }
      if (($field_info = FieldStorageConfig::loadByName('node', $field_name))) {
        if (!empty($field_info->getCardinality()) && ($field_info->getCardinality() == -1 || $field_info->getCardinality() > 1)) {
          \Drupal::service('entity_field.manager')->getBaseFieldDefinitions('node');
          $options[$field_name] = $label . ' (' . $field_name . ')';
          $form['permutation_limit'][$field_name] = array(
            '#type' => 'textfield',
            '#title' => $label,
            '#size' => 4,
            '#maxlength' => 10,
            '#states' => array(
              'visible' => array(
                ':input[name$="[denormalization_field][' . $field_name . ']"]' => array('checked' => TRUE),
              ),
            ),
            '#default_value' => !empty($this->configuration['permutation_limit'][$field_name]) ? $this->configuration['permutation_limit'][$field_name] : NULL,
          );
        }
      }
    }
    $form['fields']['#options'] = $options;

    // Re-use but modify the default form element.
    $form['fields']['#type'] = 'checkboxes';
    unset($form['fields']['#attributes']);

    $form['denormalization_field'] = $form['fields'];
    $form['fields']['#access'] = FALSE;

    $form['denormalization_field'] = array(
        '#title' => t('The field to use to denormalize the items to index.'),
        '#description' => t('The field hast to be selected for indexing to use it for denormalization.'),
        '#default_value' => isset($this->configuration['denormalization_field']) ? $this->configuration['denormalization_field'] : NULL,
      ) + $form['denormalization_field'];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function alterIndexedItems(array &$items) {
    // Reduce item data to its denormalized data set.
    foreach ($items as $item_id => $item) {
      $fields = $this->getDenormalizationFields();

      foreach ($fields as $field_name => $permutation_limit) {
        /* @var \Drupal\search_api\Item\Field $field */
        $field = $item->getField($field_name);

          foreach ($field->getValues() as $key => $value) {
            if ($key === $permutation_limit && $permutation_limit !== 0){
              break;
            }
            $items[$item_id . $key] = $this->createDocument($item, $field_name, $value);
          }
          if (!empty($field->getValues())) {
            // Unset main normalized item if field to normalize on is not empty.
            unset($items[$item_id]);
          }
      }
    }
  }

  /**
   * Returns the fields to denormalize on.
   *
   * @return array
   *   Associative list of fields to use for denormalization. The value in the
   *   array defines the permutation limit. 0 means no limit.
   */
  public function getDenormalizationFields() {
    $fields = &drupal_static(__FUNCTION__, array());
    if (empty($fields)) {
      $fields = array_filter($this->configuration['denormalization_field']);
      foreach ($fields as $field_name => $field) {
        $fields[$field_name] = 0;
        if (!empty($this->configuration['permutation_limit'][$field]) && is_numeric($this->configuration['permutation_limit'][$field])) {
          $fields[$field_name] = (int) $this->configuration['permutation_limit'][$field];
        }
      }
    }
    return $fields;
  }

  /**
   * Create a denormalized item for indexing.
   *
   * @param \Drupal\search_api\Item\Item $item
   *   The item to index.
   * @param string $field_name
   *   The field name
   * @param $value
   *   The value of the field
   *
   * @return \Drupal\search_api\Item\Item
   *   Denormalized item to index.
   */
  protected function createDocument(Item $item, $field_name, $value) {
    $item = clone $item;
    $item->getField($field_name)->setValues(array($value));
    return $item;
  }

  /**
   * {@inheritdoc}
   */
  protected function testType($type) {
    return $this->getDataTypeHelper()
      ->isTextType($type, ['text', 'string', 'integer']);
  }
}